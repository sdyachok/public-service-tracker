<?php

namespace App\Filament\Widgets;

use App\Models\Apartment;
use App\Models\Provider;
use App\Models\Record;
use App\Models\Scopes\CurrentUserScope;
use App\Models\User;
use Carbon\Carbon;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class StatsOverview extends BaseWidget
{
    protected static ?string $pollingInterval = null;

    protected function getCards(): array
    {
        $totalUsers = User::count();
        $totalApartments = Apartment::withoutGlobalScope(CurrentUserScope::class)->count();
        $totalProviders = Provider::withoutGlobalScope(CurrentUserScope::class)->count();
        $totalRecords = Record::withoutGlobalScope(CurrentUserScope::class)->count();

        $start = Carbon::now()->subMonths(6)->startOfMonth();
        $end = Carbon::now();

        $result = Record::withoutGlobalScope(CurrentUserScope::class)
            ->selectRaw('count(id) as numbers_of_records')
            ->whereBetween('month', [$start, $end])
            ->groupByRaw("date_trunc('month', created_at)")
            ->get()
            ->toArray();

        $recordsPerMonth = collect($result)->flatten()->all();

        return [
            Card::make('Total users', $totalUsers),
            Card::make('Total apartments', $totalApartments),
            Card::make('Total providers', $totalProviders),
            Card::make('Total records', $totalRecords)
                ->chart($recordsPerMonth),
        ];
    }
}
