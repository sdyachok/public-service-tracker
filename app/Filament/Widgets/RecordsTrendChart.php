<?php

namespace App\Filament\Widgets;

use App\Models\Record;
use App\Models\Scopes\CurrentUserScope;
use Carbon\Carbon;
use Filament\Widgets\BarChartWidget;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class RecordsTrendChart extends BarChartWidget
{
    protected static ?string $heading = 'Records created';

    protected static ?string $pollingInterval = null;

    protected static ?string $maxHeight = '300px';

    protected static ?array $options = [
        'plugins' => [
            'legend' => [
                'display' => false,
            ],
        ],
        'scales' => [
            'y' => [
                'display' => false,
            ],
            'x' => [
                'grid' => [
                    'display' => false,
                ],
            ],
        ],
    ];

    protected int | string | array $columnSpan = 'full';

    protected function getData(): array
    {
        $data = Trend::query(Record::withoutGlobalScope(CurrentUserScope::class))
            ->between(
                start: now()->subMonths(12)->startOfMonth(),
                end: now()->endOfMonth()
            )
            ->perMonth()
            ->count('id');

        return [
            'datasets' => [
                [
                    'data' => $data->map(fn (TrendValue $item) => $item->aggregate)->toArray(),
                    'backgroundColor' => 'rgba(54, 162, 235, 0.4)',
                    'borderColor' => 'rgb(54, 162, 235)',
                    'borderWidth' => 1,
                ],
            ],
            'labels' => $data->map(fn (TrendValue $item) => Carbon::make($item->date)->format('M Y'))->toArray(),
        ];
    }
}
