<?php

namespace App\Models;

use App\Models\Scopes\CurrentUserScope;
use App\Support\Traits\HasUser;
use Database\Factories\RecordFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Record
 *
 * @property int $id
 * @property int $user_id
 * @property int $apartment_id
 * @property int $provider_id
 * @property Carbon $month
 * @property int $start_numbers
 * @property int $end_numbers
 * @property string $total_to_pay
 * @property string $total_paid
 * @property bool $is_paid
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Apartment $apartment
 * @property-read Provider $provider
 * @property-read User $user
 *
 * @method static RecordFactory factory(...$parameters)
 * @method static Builder|Record newModelQuery()
 * @method static Builder|Record newQuery()
 * @method static Builder|Record query()
 * @mixin Eloquent
 */
class Record extends Model
{
    use HasFactory;
    use HasUser;

    protected $guarded = [];

    protected $casts = [
        'is_paid' => 'boolean',
        'total_to_pay' => 'decimal:2',
        'total_paid' => 'decimal:2',
        'month' => 'date',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new CurrentUserScope());
    }

    // Relations
    public function apartment(): BelongsTo
    {
        return $this->belongsTo(Apartment::class);
    }

    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }
}
