<?php

namespace App\Models;

use Illuminate\Support\Str;

enum ProviderType: string
{
    case Electricity = 'electricity';
    case Water = 'water';
    case NaturalGas = 'natural_gas';
    case Other = 'other';

    public static function values(): array
    {
        return array_combine(
            array_column(self::cases(), 'value'),
            collect(array_column(self::cases(), 'name'))->map(fn ($value) => Str::headline($value))->toArray(),
        );
    }
}
