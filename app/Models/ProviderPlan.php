<?php

namespace App\Models;

use App\Models\Scopes\CurrentUserScope;
use App\Support\Traits\HasUser;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProviderPlan
 *
 * @property int $id
 * @property int $user_id
 * @property int $provider_id
 * @property string $price
 * @property Carbon $started_at
 * @property bool $uses_apartment_area
 * @property bool $is_active
 * @property bool $is_fixed_price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Provider $provider
 * @property-read User $user
 *
 * @method static Builder|ProviderPlan newModelQuery()
 * @method static Builder|ProviderPlan newQuery()
 * @method static Builder|ProviderPlan query()
 * @mixin Eloquent
 */
class ProviderPlan extends Model
{
    use HasFactory;
    use HasUser;

    protected $guarded = [];

    protected $casts = [
        'price' => 'decimal:4',
        'started_at' => 'date',
        'uses_apartment_area' => 'boolean',
        'is_active' => 'boolean',
        'is_fixed_price' => 'boolean',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new CurrentUserScope());
    }

    // Relations
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }
}
