<?php

namespace App\Models;

use App\Models\Scopes\CurrentUserScope;
use App\Support\Traits\HasUser;
use Database\Factories\ApartmentFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Apartment
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $location
 * @property string $area_size
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @property-read Collection|Provider[] $providers
 * @property-read int|null $providers_count
 * @property-read Collection|Record[] $records
 * @property-read int|null $records_count
 *
 * @method static ApartmentFactory factory(...$parameters)
 * @method static Builder|Apartment newModelQuery()
 * @method static Builder|Apartment newQuery()
 * @method static Builder|Apartment query()
 * @mixin Eloquent
 */
class Apartment extends Model
{
    use HasFactory;
    use HasUser;

    protected $guarded = [];

    protected $casts = [
        'area_size' => 'decimal:2',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new CurrentUserScope());
    }

    // Relations
    public function providers(): HasMany
    {
        return $this->hasMany(Provider::class);
    }

    public function records(): HasMany
    {
        return $this->hasMany(Record::class);
    }

    // Helpers and getters
}
