<?php

namespace App\Models;

use App\Models\Scopes\CurrentUserScope;
use App\Support\Traits\HasUser;
use Database\Factories\ProviderFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Provider
 *
 * @property int $id
 * @property int $user_id
 * @property int $apartment_id
 * @property string $name
 * @property bool $is_active
 * @property string|null $account_number
 * @property string|null $website
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Apartment $apartment
 * @property-read User $user
 * @property-read Collection|Record[] $records
 * @property-read int|null $records_count
 * @property-read Collection|ProviderPlan[] $plans
 * @property-read int|null $plans_count
 *
 * @method static ProviderFactory factory(...$parameters)
 * @method static Builder|Provider newModelQuery()
 * @method static Builder|Provider newQuery()
 * @method static Builder|Provider query()
 * @mixin Eloquent
 */
class Provider extends Model
{
    use HasFactory;
    use HasUser;

    protected $guarded = [];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new CurrentUserScope());
    }

    // Relations
    public function apartment(): BelongsTo
    {
        return $this->belongsTo(Apartment::class);
    }

    public function records(): HasMany
    {
        return $this->hasMany(Record::class);
    }

    public function plans(): HasMany
    {
        return $this->hasMany(ProviderPlan::class);
    }

    // Helpers and getters

    /**
     * @return Collection|Provider[]
     */
    public static function getForCurrentUser(): Collection|array
    {
        return static::with('apartment')->get();
    }
}
