<?php

namespace App\Policies;

use App\Models\Provider;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProviderPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Provider $provider): bool
    {
        return $provider->isOwner($user);
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Provider $provider): bool
    {
        return $provider->isOwner($user);
    }

    public function delete(User $user, Provider $provider): bool
    {
        return $provider->isOwner($user);
    }

    public function restore(User $user, Provider $provider): bool
    {
        return $provider->isOwner($user);
    }

    public function forceDelete(User $user, Provider $provider): bool
    {
        return $provider->isOwner($user);
    }
}
