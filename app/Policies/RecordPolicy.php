<?php

namespace App\Policies;

use App\Models\Record;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecordPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Record $record): bool
    {
        return $record->isOwner($user);
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Record $record): bool
    {
        return $record->isOwner($user);
    }

    public function delete(User $user, Record $record): bool
    {
        return $record->isOwner($user);
    }

    public function restore(User $user, Record $record): bool
    {
        return $record->isOwner($user);
    }

    public function forceDelete(User $user, Record $record): bool
    {
        return $record->isOwner($user);
    }
}
