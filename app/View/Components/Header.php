<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Header extends Component
{
    protected array $links = [
        'pages.index' => 'Home',
    ];

    public function __construct()
    {
    }

    public function render(): View
    {
        return view('components.header', ['links' => $this->links]);
    }
}
