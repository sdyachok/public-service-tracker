<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'apartment_id' => $this->apartment_id,
            'name' => $this->name,
            'account_number' => $this->account_number,
            'provider_type' => $this->provider_type,
            'is_active' => $this->is_active,
            'website' => $this->website,
            'apartment' => ApartmentResource::make($this->whenLoaded('apartment')),
            'plans' => ProviderPlanResource::collection($this->whenLoaded('plans')),
        ];
    }
}
