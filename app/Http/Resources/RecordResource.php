<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RecordResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'apartment_id' => $this->apartment_id,
            'apartment' => ApartmentResource::make($this->whenLoaded('apartment')),
            'provider_id' => $this->provider_id,
            'provider' => ProviderResource::make($this->whenLoaded('provider')),
            'month' => $this->month->toDateString(),
            'start_numbers' => $this->start_numbers,
            'end_numbers' => $this->end_numbers,
            'total_to_pay' => $this->total_to_pay,
            'total_paid' => $this->total_paid,
            'is_paid' => $this->is_paid,
            'created_at' => $this->created_at,
        ];
    }
}
