<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderPlanResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'price' => $this->price,
            'started_at' => $this->started_at,
            'uses_apartment_area' => $this->uses_apartment_area,
            'is_active' => $this->is_active,
            'is_fixed_price' => $this->is_fixed_price,
            'created_at' => $this->created_at,
        ];
    }
}
