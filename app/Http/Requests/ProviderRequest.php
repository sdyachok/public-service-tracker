<?php

namespace App\Http\Requests;

use App\Models\ProviderType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class ProviderRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'apartment_id' => ['required', 'exists:apartments,id'],
            'name' => ['required', 'string', 'max:100'],
            'is_active' => ['boolean'],
            'account_number' => ['nullable', 'string', 'max:15'],
            'website' => ['nullable', 'string', 'url'],
            'provider_type' => ['string', new Enum(ProviderType::class)],
        ];
    }
}
