<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProviderPlanRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'provider_id' => ['required', 'exists:providers,id'],
            'price' => ['required', 'numeric', 'min:0'],
            'started_at' => ['required', 'date'],
            'uses_apartment_area' => ['boolean'],
            'is_active' => ['boolean'],
            'is_fixed_price' => ['boolean'],
        ];
    }
}
