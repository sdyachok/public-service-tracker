<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'apartment_id' => ['required', 'exists:apartments,id'],
            'provider_id' => ['required', 'exists:providers,id'],
            'month' => ['required', 'date'],
            'start_numbers' => ['required', 'integer', 'min:0'],
            'end_numbers' => ['required', 'integer', 'min:0'],
            'total_to_pay' => ['numeric', 'min:0'],
            'total_paid' => ['numeric', 'min:0'],
            'is_paid' => ['boolean'],
        ];
    }
}
