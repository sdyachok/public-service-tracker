<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecordRequest;
use App\Http\Resources\ApartmentResource;
use App\Http\Resources\ProviderResource;
use App\Http\Resources\RecordResource;
use App\Models\Apartment;
use App\Models\Provider;
use App\Models\Record;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Response;

class RecordsController extends Controller
{
    public function index(Request $request): Response
    {
        $apartments = Apartment::get();
        $providers = Provider::with(['apartment', 'plans'])->get();

        $records = Record::with(['apartment', 'provider', 'provider.apartment'])
            ->when($request->input('apartment_id'), fn (Builder $query, $apartmentId) => $query->where('apartment_id', $apartmentId))
            ->when($request->input('provider_id'), fn (Builder $query, $providerId) => $query->where('provider_id', $providerId))
            ->when($request->input('paid'), fn (Builder $query) => $query->where('is_paid', $request->boolean('paid')))
            ->orderByDesc('created_at')
            ->paginate(25)
            ->appends($request->query());

        return inertia('Records', [
            'apartments' => ApartmentResource::collection($apartments),
            'providers' => ProviderResource::collection($providers),
            'records' => RecordResource::collection($records),
            'filters' => $request->only(['apartment_id', 'provider_id', 'paid']),
        ]);
    }

    public function store(RecordRequest $request): RedirectResponse
    {
        Record::create(array_merge($request->validated(), [
            'user_id' => auth()->id(),
        ]));

        return back()->with('success', 'Record has been successfully added');
    }

    public function update(RecordRequest $request, Record $record): RedirectResponse
    {
        $record->update($request->validated());

        return back()->with('success', 'Record has been successfully updated');
    }

    public function addPayment(Request $request, Record $record): RedirectResponse
    {
        $attributes = $request->validate([
            'total_paid' => ['required', 'numeric', 'min:0'],
        ]);

        $record->update(array_merge($attributes, ['is_paid' => true]));

        return back()->with('success', 'Payment has been successfully added');
    }

    public function destroy(Record $record): RedirectResponse
    {
        $record->delete();

        return back()->with('success', 'Record has been successfully removed');
    }
}
