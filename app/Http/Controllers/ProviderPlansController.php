<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProviderPlanRequest;
use App\Models\ProviderPlan;

class ProviderPlansController extends Controller
{
    public function store(ProviderPlanRequest $request)
    {
        $attributes = $request->validated();

        ProviderPlan::create(array_merge($attributes, ['user_id' => auth()->id()]));

        return back()->with('success', 'Provider plan has been successfully created');
    }

    public function update(ProviderPlanRequest $request, ProviderPlan $plan)
    {
        $plan->update($request->validated());

        return back()->with('success', 'Provider plan has been successfully updated');
    }
}
