<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProviderRequest;
use App\Http\Resources\ApartmentResource;
use App\Http\Resources\ProviderPlanResource;
use App\Http\Resources\ProviderResource;
use App\Http\Resources\RecordResource;
use App\Models\Apartment;
use App\Models\Provider;
use Inertia\Response;

class ProvidersController extends Controller
{
    public function index(): Response
    {
        $apartments = Apartment::get();
        $providers = Provider::getForCurrentUser();

        return inertia('Providers', [
            'apartments' => ApartmentResource::collection($apartments),
            'providers' => ProviderResource::collection($providers),
        ]);
    }

    public function store(ProviderRequest $request)
    {
        Provider::create(array_merge($request->validated(), [
            'user_id' => auth()->id(),
        ]));

        return back()->with('success', 'Provider has been successfully added');
    }

    public function show(Provider $provider)
    {
        $apartment = $provider->apartment()->first();

        $provider->load('plans');

        return inertia('ProviderDetails', [
            'apartment' => ApartmentResource::make($apartment),
            'apartments' => ApartmentResource::collection([$apartment]),
            'provider' => ProviderResource::make($provider),
            'plans' => ProviderPlanResource::collection($provider->plans()->get()),
            'providers' => ProviderResource::collection([$provider]),
            'records' => RecordResource::collection(
                $provider->records()
                    ->with(['apartment', 'provider'])
                    ->orderByDesc('created_at')
                    ->take(10)
                    ->get(),
            ),
        ]);
    }

    public function update(ProviderRequest $request, Provider $provider)
    {
        $provider->update($request->validated());

        return back()->with('success', 'Provider has been successfully updated');
    }

    public function destroy(Provider $provider)
    {
        $provider->delete();

        return to_route('providers.index')->with('success', 'Provider has been successfully removed');
    }
}
