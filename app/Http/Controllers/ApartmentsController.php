<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApartmentRequest;
use App\Http\Resources\ApartmentResource;
use App\Http\Resources\ProviderResource;
use App\Http\Resources\RecordResource;
use App\Models\Apartment;
use Illuminate\Http\RedirectResponse;
use Inertia\Response;

class ApartmentsController extends Controller
{
    public function index(): Response
    {
        $apartments = Apartment::get();

        return inertia('Apartments', [
            'apartments' => ApartmentResource::collection($apartments),
        ]);
    }

    public function show(Apartment $apartment): Response
    {
        return inertia('ApartmentDetails', [
            'apartment' => ApartmentResource::make($apartment),
            'apartments' => ApartmentResource::collection([$apartment]),
            'providers' => ProviderResource::collection(
                $apartment->providers()->with('plans')->orderBy('name')->get()
            ),
            'records' => RecordResource::collection(
                $apartment->records()
                    ->with(['apartment', 'provider'])
                    ->orderByDesc('created_at')
                    ->take(10)
                    ->get(),
            ),
        ]);
    }

    public function store(ApartmentRequest $request): RedirectResponse
    {
        $data = $request->validated();

        Apartment::create(array_merge($data, ['user_id' => auth()->id()]));

        return to_route('apartments.index')
            ->with('success', 'Apartment has been successfully added');
    }

    public function update(ApartmentRequest $request, Apartment $apartment): RedirectResponse
    {
        $apartment->update($request->validated());

        return back()
            ->with('success', 'Apartments has been successfully updated');
    }

    public function destroy(Apartment $apartment): RedirectResponse
    {
        $apartment->delete();

        return to_route('apartments.index')
            ->with('success', 'Apartments has been successfully removed');
    }
}
