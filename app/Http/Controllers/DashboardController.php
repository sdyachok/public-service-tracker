<?php

namespace App\Http\Controllers;

use App\Http\Resources\RecordResource;
use App\Models\Provider;
use App\Models\ProviderType;
use App\Models\Record;
use App\Models\Scopes\CurrentUserScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Inertia\Response;

class DashboardController extends Controller
{
    public function __invoke(Request $request): Response
    {
        $latestRecords = Record::query()
            ->with(['apartment', 'provider'])
            ->orderByDesc('created_at')
            ->take(5)
            ->get();

        $start = Carbon::now()->subMonths(12);
        $end = Carbon::now();

        /** @var $paymentsByMonths Collection */
        $paymentsByMonths = Record::query()
            ->selectRaw("date_trunc('month', month) as period, sum(total_to_pay) as amount ")
            ->whereBetween('month', [$start, $end])
            ->groupByRaw("date_trunc('month', month)")
            ->orderByRaw("date_trunc('month', month)")
            ->withCasts(['period' => 'datetime'])
            ->get();

        $start = Carbon::now()->startOfMonth();
        $monthTotals = Record::query()
            ->selectRaw('sum(total_to_pay) as total_to_pay, sum(total_paid) as total_paid')
            ->whereBetween('created_at', [$start, $end])
            ->first()
            ->toArray();

        $currentMonthPaymentsByType = Provider::withoutGlobalScope(CurrentUserScope::class)
            ->select('providers.provider_type as provider_type')
            ->selectRaw('sum(records.total_paid) as total_paid')
            ->join('records', 'providers.id', '=', 'records.provider_id')
            ->whereBetween('records.created_at', [now()->startOfMonth(), now()->endOfMonth()])
            ->where('providers.user_id', Auth::id())
            ->groupBy('providers.provider_type')
            ->havingRaw('sum(records.total_paid) > ?', [0])
            ->withCasts([
                'provider_type' => ProviderType::class,
            ])
            ->get();

        $stats = [
            'payments_by_months' => [
                'labels' => $paymentsByMonths->map(fn ($item) => $item->period->format('M Y'))->toArray(),
                'data' => $paymentsByMonths->map(fn ($item) => $item->amount)->toArray(),
            ],
            'current_month_payments_by_type' => [
                'labels' => $currentMonthPaymentsByType->map(fn ($item) => Str::headline($item->provider_type->name))->toArray(),
                'data' => $currentMonthPaymentsByType->map(fn ($item) => $item->total_paid)->toArray(),
            ],
            'month_totals' => $monthTotals,
        ];

        return inertia('Dashboard', [
            'latest_records' => RecordResource::collection($latestRecords),
            'stats' => $stats,
        ]);
    }
}
