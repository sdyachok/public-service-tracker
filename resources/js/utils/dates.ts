import { titleCase } from "@/utils/texts";

export const getMonthName = (date: string) => titleCase(new Date(date).toLocaleString("default", { month: "long" }));

export const formatDateShort = (date: string) =>
  new Date(date).toLocaleDateString(undefined, {
    month: "long",
    year: "numeric",
    day: "numeric",
  });
