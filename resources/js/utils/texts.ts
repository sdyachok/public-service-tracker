/**
 * Makes first letter uppercase
 */
export const titleCase = (value: string): string => {
  if (!value) return "";

  return value.charAt(0).toUpperCase() + value.slice(1);
};
