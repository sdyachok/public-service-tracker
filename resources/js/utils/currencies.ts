export const formatCurrency = (amount: string): string => {
  // @todo: add currency to the list of arguments
  const formatter = new Intl.NumberFormat(undefined, {
    style: "currency",
    currency: "UAH",

    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  return formatter.format(parseFloat(amount));
};
