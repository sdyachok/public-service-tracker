import "./bootstrap";
import "../css/app.css";

import {
  Chart,
  Colors,
  Title as ChartTitle,
  Tooltip,
  Legend,
  ArcElement,
  BarElement,
  LineElement,
  CategoryScale,
  LinearScale,
} from "chart.js";

import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue3";
import { InertiaProgress } from "@inertiajs/progress";
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
// @ts-ignore
import { ZiggyVue } from "../../vendor/tightenco/ziggy/dist/vue.m";

// @ts-ignore
Chart.register(Colors, ChartTitle, Tooltip, Legend, ArcElement, BarElement, LineElement, CategoryScale, LinearScale);

const appName = window.document.getElementsByTagName("title")[0]?.innerText || "Laravel";

createInertiaApp({
  title: (title) => `${title} - ${appName}`,
  // @ts-ignore
  resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob("./Pages/**/*.vue")),
  setup({ el, app, props, plugin }) {
    const vueApp = createApp({ render: () => h(app, props) })
      .use(plugin)
      .use(ZiggyVue, Ziggy);

    vueApp.mount(el);

    return vueApp;
  },
}).then((_) => {});

InertiaProgress.init({ color: "#4B5563" });
