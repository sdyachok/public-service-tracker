type PaginationLinks = {
  first: string | null;
  last: string | null;
  next: string | null;
  prev: string | null;
};

type PaginationLink = {
  active: boolean;
  label: string;
  url: string | null;
};

declare type PaginationMeta = {
  current_page: number;
  from: number;
  last_page: number;
  links: PaginationLink[];
  path: string;
  per_page: number;
  to: number;
  total: number;
};

declare interface PaginatedResource<T> {
  data: T[];
  links: PaginationLinks;
  meta: PaginationMeta;
}

declare interface Apartment {
  id: number;
  name: string;
  location: string;
  area_size: string;
}

declare type OptionalApartment = Apartment | null;

declare type ApartmentData = Omit<Apartment, "id">;

declare interface ProviderPlan {
  id: number;
  provider_id: number;
  price: string;
  uses_apartment_area: boolean;
  is_active: boolean;
  is_fixed_price: boolean;
  started_at: string;
  created_at: string;
}

declare type ProviderPlanData = Omit<ProviderPlan, "id" | "provider_id" | "created_at">;

declare type ProviderTypes = {
  [index: string]: string;
};

declare interface Provider {
  id: number;
  apartment_id: number;
  name: string;
  account_number: string;
  is_active: boolean;
  website: string;
  provider_type: string;

  apartment: Apartment | null;
  plans?: ProviderPlan[];
}

declare type OptionalProvider = Provider | null;

declare type ProviderData = Omit<Provider, "id" | "apartment" | "is_active" | "apartment_id" | "plans"> & {
  apartment_id: string;
};

declare interface ServiceRecord {
  id: number;
  apartment_id: number;
  provider_id: number;
  month: string;
  start_numbers: number;
  end_numbers: number;
  total_to_pay: string;
  total_paid: string;
  is_paid: boolean;
  created_at: string;

  apartment?: Apartment;
  provider?: Provider;
}

declare interface RecordData {
  apartment_id: string;
  provider_id: string;
  month: string;
  start_numbers: string;
  end_numbers: string;
  total_to_pay: string;
  total_paid: string;
  is_paid: boolean;
}

declare interface RecordFilters {
  apartment_id?: string;
  provider_id?: string;
  paid?: string;
}
