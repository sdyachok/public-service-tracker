<header class="py-10">
  <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
    <nav class="flex justify-between">
      <div class="flex items-center md:gap-x-12">
        <a href="{{ route('pages.index')  }}" class="text-xl text-slate-600 font-bold font-serif tracking-tight">
          Public <span class="text-primary-600">Service</span> Tracker
        </a>

        <div class="flex gap-x-4 md:gap-x-6">
          @foreach($links as $to => $title)
            <a class="inline-block rounded-lg py-1 px-2 text-sm text-slate-700 hover:bg-slate-100 hover:text-slate-900"
               href="{{ route($to) }}">{{ $title }}</a>
          @endforeach
        </div>
      </div>

      <div class="flex items-center gap-x-5 md:gap-x-8">
        @guest
          <a class="inline-block rounded-lg py-1 px-2 text-sm text-slate-700 hover:bg-slate-100 hover:text-slate-900"
             href="{{ route('login') }}">Sign in</a>

          <a class="inline-flex items-center justify-center rounded-full py-2 px-4 text-sm font-medium focus:outline-none focus-visible:outline-2 focus-visible:outline-offset-2 bg-primary-600 text-white hover:text-slate-100 hover:bg-primary-700 active:bg-primary-800 active:text-primary-100 focus-visible:outline-primary-600"
             href="{{ route('register') }}">Sign up for free</a>
        @else
          <a class="inline-flex items-center justify-center rounded-full py-2 px-4 text-sm font-medium focus:outline-none focus-visible:outline-2 focus-visible:outline-offset-2 bg-primary-600 text-white hover:text-slate-100 hover:bg-primary-700 active:bg-primary-800 active:text-primary-100 focus-visible:outline-primary-600"
             href="{{ route('dashboard') }}">Dashboard</a>
        @endauth
      </div>
    </nav>
  </div>
</header>
