<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title inertia>{{ config('app.name', 'Laravel') }}</title>

  <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;500;600;700&display=swap">

  @routes
  @vite('resources/js/app.ts')
  @inertiaHead
</head>
<body class="bg-gray-100 text-base text-gray-900 font-sans antialiased">
  @inertia
</body>
</html>
