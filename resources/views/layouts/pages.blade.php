<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>Public Service Tracker</title>

  @vite('resources/css/site.css')
</head>
<body class="text-base text-gray-900 font-sans antialiased flex flex-col h-full min-h-screen">
  <x-header />

  <main class="flex-grow">
    @yield('content')
  </main>

  <x-footer />
</body>
</html>
