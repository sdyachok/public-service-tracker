<?php

use App\Http\Controllers\ApartmentsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProviderPlansController;
use App\Http\Controllers\ProvidersController;
use App\Http\Controllers\RecordsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'index'])->name('pages.index');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/dashboard', DashboardController::class)->name('dashboard');

    Route::resource('apartments', ApartmentsController::class)->except(['create', 'edit']);
    Route::resource('providers', ProvidersController::class)->except(['create', 'edit']);
    Route::resource('plans', ProviderPlansController::class)->only(['store', 'update']);

    Route::post('records/{record}/payment', [RecordsController::class, 'addPayment'])->name('records.payment');
    Route::resource('records', RecordsController::class)->except(['show', 'create', 'edit']);
});

require __DIR__.'/auth.php';
