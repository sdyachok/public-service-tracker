<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index()
                ->constrained()->cascadeOnDelete();
            $table->foreignId('apartment_id')->index()
                ->constrained()->cascadeOnDelete();
            $table->foreignId('provider_id')->index()
                ->constrained()->cascadeOnDelete();
            $table->date('month'); // Any date in a month
            $table->integer('start_numbers')->default(0);
            $table->integer('end_numbers')->default(0);
            $table->decimal('total_to_pay', 10)->default(0);
            $table->decimal('total_paid', 10)->default(0);
            $table->boolean('is_paid')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('records');
    }
};
