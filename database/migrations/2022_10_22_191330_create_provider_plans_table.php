<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('provider_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index()
                ->constrained()->cascadeOnDelete();
            $table->foreignId('provider_id')->index()
                ->constrained()->cascadeOnDelete();
            $table->decimal('price', 10, 4);
            $table->date('started_at');
            $table->boolean('uses_apartment_area')->default(false);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('provider_plans');
    }
};
