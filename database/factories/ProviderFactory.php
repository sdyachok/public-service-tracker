<?php

namespace Database\Factories;

use App\Models\Apartment;
use App\Models\Provider;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Provider>
 */
class ProviderFactory extends Factory
{
    public function definition(): array
    {
        $userId = $this->faker->randomElement(User::all()->pluck('id')->toArray());

        return [
            'user_id' => $userId,
            'apartment_id' => $this->faker->randomElement(
                Apartment::where('user_id', $userId)->get()->pluck('id')->toArray()
            ),
            'name' => $this->faker->randomElement(['Electricity', 'Water', 'Natural Gas', 'Waste', 'House service']),
            'is_active' => true,
            'account_number' => $this->faker->numerify('#######'),
            'website' => $this->faker->url(),
        ];
    }
}
