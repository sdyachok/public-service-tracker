# Ideas and todos

### Deployment

-   [x] Deploy to hosting (Fly.io)

### General

-   [ ] Fix go-back button
-   [ ] Add flash/toast messages

### Dashboard

-   [x] Latest records
-   [x] "Current" month payments (planned and total)
-   [x] Charts
    -   [x] Payments per provider type (current month)
    -   [x] Monthly usage (last 12 months)

### Apartments

-   [x] Add area (in square meters) to apartments
-   [x] "Edit" on detail page
-   [x] "Remove" on detail page
-   [ ] Reload data after pressing `Back` in browser

### Providers

-   [ ] "Have counters" marker
-   [x] Provider type, e.g. 'Gas', 'Water', etc.
-   [x] Provider tariffs with current and historical
    -   [x] Usage-based tariffs
    -   [ ] Apartment area-based tariffs
    -   [x] Fixed-price tariffs
    -   [ ] Current/active tariff (as separate field)
    -   [x] FIX: change tariff precision in UI (4 digits)
-   [x] Latest paid records and current tariff on details page
-   [x] "Edit" on detail page
-   [x] "Remove" on detail page

### Records

-   [x] Automatic calculation of `Total to pay` based on active tariff
-   [x] Filters
    -   [x] By status (paid/unpaid)
    -   [x] By apartment
    -   [x] By provider
-   [x] Pagination

### User profile

-   [ ] User profile page
    -   [ ] Edit basic information (name, email)
    -   [ ] Change password

### Pages

-   [x] Home page/landing page design and content
-   [ ] About and terms and conditions
